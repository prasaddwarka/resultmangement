package com.example.ResultManagement.Helper;
import com.example.ResultManagement.Models.Student;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class StudentMapper implements RowMapper<Student>{
    public Student mapRow(ResultSet rs, int rowNum) throws SQLException{
        Student student = new Student();
        student.setName(rs.getString("sname"));
        student.setRollNo(rs.getString("rollno"));
        student.setEmail(rs.getString("semail"));
        student.setGender(rs.getString("gender"));
        student.setDOB(rs.getString("sdob"));
        student.setClassID(rs.getInt("classid"));
        return student;
    }
}
