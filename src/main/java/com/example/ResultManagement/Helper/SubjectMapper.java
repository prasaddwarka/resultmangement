package com.example.ResultManagement.Helper;
import com.example.ResultManagement.Models.Student;
import com.example.ResultManagement.Models.Subject;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class SubjectMapper implements RowMapper<Subject>{
    public Subject mapRow(ResultSet rs, int rowNum) throws SQLException{
        Subject subject = new Subject();
        subject.setId(rs.getInt("id"));
        subject.setSubname(rs.getString("subname"));
        subject.setScode(rs.getString("scode"));
        return subject;
    }
}
