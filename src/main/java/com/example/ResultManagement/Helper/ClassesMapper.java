package com.example.ResultManagement.Helper;

import com.example.ResultManagement.Models.Classes;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class ClassesMapper implements RowMapper<Classes>{
    public Classes mapRow(ResultSet rs, int rowNum) throws SQLException{
        Classes Class = new Classes();
        Class.setId(rs.getInt("classid"));
        Class.setCname(rs.getString("cname"));
        Class.setCnameneu(rs.getInt("cnameneu"));
        Class.setSection(rs.getString("section"));
        return Class;
    }
}
