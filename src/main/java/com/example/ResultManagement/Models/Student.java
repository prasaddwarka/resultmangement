package com.example.ResultManagement.Models;

public class Student{
    private String Name;
    private String RollNo;
    private String Email;
    private String Gender;
    private String DOB;
    private int ClassID;

    public Student() {
    }



    public Student(String name, String rollNo, String email, String gender, String DOB, int classID) {
        Name = name;
        RollNo = rollNo;
        Email = email;
        Gender = gender;
        this.DOB = DOB;
        ClassID = classID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getRollNo() {
        return RollNo;
    }

    public void setRollNo(String rollNo) {
        RollNo = rollNo;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getDOB() {
        return DOB;
    }

    public void setDOB(String DOB) {
        this.DOB = DOB;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }
}
