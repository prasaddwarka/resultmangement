package com.example.ResultManagement.Models;

public class Subject{
    private int id;
    private String subname;
    private String scode;

    public Subject() {
    }

    public Subject(int id, String subname, String scode) {
        this.id = id;
        this.subname = subname;
        this.scode = scode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubname() {
        return subname;
    }

    public void setSubname(String subname) {
        this.subname = subname;
    }

    public String getScode() {
        return scode;
    }

    public void setScode(String scode) {
        this.scode = scode;
    }
}
