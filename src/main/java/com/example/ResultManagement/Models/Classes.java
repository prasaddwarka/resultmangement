package com.example.ResultManagement.Models;

public class Classes {
    private int id;
    private String cname;
    private int cnameneu;
    private String section;

    public Classes() {
    }

    public Classes(String cname, int cnameneu, String section) {
        this.cname = cname;
        this.cnameneu = cnameneu;
        this.section = section;
    }

    public Classes(int id, String cname, int cnameneu, String section) {
        this.id = id;
        this.cname = cname;
        this.cnameneu = cnameneu;
        this.section = section;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    public int getCnameneu() {
        return cnameneu;
    }

    public void setCnameneu(int cnameneu) {
        this.cnameneu = cnameneu;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
