package com.example.ResultManagement.Models;

public class Results{
    private String StudentRoll;
    private int ClassID;
    private int SubjectID;
    private int Marks;

    public Results() {
    }

    public Results(String studentRoll, int classID, int subjectID, int marks) {
        StudentRoll = studentRoll;
        ClassID = classID;
        SubjectID = subjectID;
        Marks = marks;
    }

    public String getStudentRoll() {
        return StudentRoll;
    }

    public void setStudentRoll(String studentRoll) {
        StudentRoll = studentRoll;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }

    public int getMarks() {
        return Marks;
    }

    public void setMarks(int marks) {
        Marks = marks;
    }
}
