package com.example.ResultManagement.Models;

public class Message {
    private String Name;
    private String Content;

    public Message() {
    }

    public Message(String name, String content) {
        Name = name;
        Content = content;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }
}
