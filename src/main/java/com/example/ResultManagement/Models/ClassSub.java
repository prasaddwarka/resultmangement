package com.example.ResultManagement.Models;

public class ClassSub{
    private int ClassID;
    private int SubjectID;

    public ClassSub() {
    }

    public ClassSub(int classID, int subjectID) {
        ClassID = classID;
        SubjectID = subjectID;
    }

    public int getClassID() {
        return ClassID;
    }

    public void setClassID(int classID) {
        ClassID = classID;
    }

    public int getSubjectID() {
        return SubjectID;
    }

    public void setSubjectID(int subjectID) {
        SubjectID = subjectID;
    }
}
