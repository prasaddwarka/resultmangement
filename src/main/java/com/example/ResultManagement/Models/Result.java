package com.example.ResultManagement.Models;

public class Result {
    private String RollNumber;
    private int ClassName;

    public Result() {
    }

    public Result(String rollNumber, int className) {
        RollNumber = rollNumber;
        ClassName = className;
    }

    public String getRollNumber() {
        return RollNumber;
    }

    public void setRollNumber(String rollNumber) {
        RollNumber = rollNumber;
    }

    public int getClassName() {
        return ClassName;
    }

    public void setClassName(int className) {
        ClassName = className;
    }
}
