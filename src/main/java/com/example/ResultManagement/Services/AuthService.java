package com.example.ResultManagement.Services;
import com.example.ResultManagement.Models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class AuthService{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public boolean checkUser(User user) {
        try {
            String Query = "Select count(*) from users where uemail=? and upass=?";
            int result = this.jdbcTemplate.queryForObject(Query, Integer.class, new Object[]{user.getEmail(), user.getPassword()});
            return result > 0;
        }catch (Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return false;
        }
    }
}
