package com.example.ResultManagement.Services;

import com.example.ResultManagement.Helper.ClassesMapper;
import com.example.ResultManagement.Helper.StudentMapper;
import com.example.ResultManagement.Helper.SubjectMapper;
import com.example.ResultManagement.Models.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class AdminService {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public List<Classes> getClasses(){
        String Query = "Select * from classes";
        List<Classes> classList = this.jdbcTemplate.query(Query, new ClassesMapper());
        return classList;
    }

    public List<Student> getStudents(){
        String Query = "Select sname,rollno,semail,gender,sdob,classid from students";
        List<Student> students = this.jdbcTemplate.query(Query,new StudentMapper());
        return students;
    }

    public List<Subject> getSubjects(){
        String Query = "select * from subjects";
        List<Subject> subjects = this.jdbcTemplate.query(Query,new SubjectMapper());
        return subjects;
    }

    public int addstudent(Student student){
        try{
            String Query = "insert into students(sname,rollno,semail,gender,sdob,classid) values(?,?,?,?,?,?)";
            int result = this.jdbcTemplate.update(Query,student.getName(),student.getRollNo(),student.getEmail(),student.getGender(), LocalDate.parse(student.getDOB()),student.getClassID());
            return result;
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public int addclass(Classes classes){
        try{
            String Query = "insert into classes(cname,cnameneu,section) values(?,?,?)";
            int result = this.jdbcTemplate.update(Query,classes.getCname(),classes.getCnameneu(),classes.getSection());
            return result;
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public int addclassSubjects(ClassSub classsub){
        try{
            String Query = "insert into subjectcombination(classid,subjectid,status) values(?,?,?)";
            int result = this.jdbcTemplate.update(Query,classsub.getClassID(),classsub.getSubjectID(),1);
            return result;
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public int addresult(Results result){
        try{
            String preCheck = "Select count(*) from subjectcombination where classid=? and subjectid=?";
            int Checker = this.jdbcTemplate.queryForObject(preCheck,Integer.class,new Object[]{result.getClassID(),result.getSubjectID()});
            if(Checker>0){
                String sql = "Select sid from students where rollno=?";
                int sid = this.jdbcTemplate.queryForObject(sql,Integer.class,new Object[]{result.getStudentRoll()});
                String Query = "insert into result(sid,classid,subjectid,marks) values(?,?,?,?)";
                int r = this.jdbcTemplate.update(Query,sid,result.getClassID(),result.getSubjectID(),result.getMarks());
                return r;
            }else{
                return 0;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }

    public int updateresult(Results result){
        try{
            String preCheck = "Select count(*) from subjectcombination where classid=? and subjectid=?";
            int Checker = this.jdbcTemplate.queryForObject(preCheck,Integer.class,new Object[]{result.getClassID(),result.getSubjectID()});
            if(Checker >0){
                String sql = "Select sid from students where rollno=?";
                int sid = this.jdbcTemplate.queryForObject(sql,Integer.class,new Object[]{result.getStudentRoll()});
                String Query = "update result set marks=? where (sid=? and classid=? and subjectid=?)";
                int r = this.jdbcTemplate.update(Query,result.getMarks(),sid,result.getClassID(),result.getSubjectID());
                return r;
            }else{
                return 0;
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            return 0;
        }
    }
}
