package com.example.ResultManagement.Services;

import com.example.ResultManagement.Helper.ClassesMapper;
import com.example.ResultManagement.Models.Classes;
import com.example.ResultManagement.Models.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class ResultService {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    public List<Classes> getAllClasses(){
        String Query = "Select * from classes";
        List<Classes> classList = this.jdbcTemplate.query(Query, new ClassesMapper());
        return classList;
    }

    public List<Map<String,Object>> fetchResult(Result resultParam){
        String Query = "select t.sname,t.rollno,t.classid,t.marks,subjectid,sub.subname from (select s.sname,s.rollno,s.classid,r.marks,subjectid from students as s join result as r on s.sid=r.sid) as t join subjects as sub on sub.id=t.subjectid where t.rollno=? and t.classid=?";
        List<Map<String,Object>> rows = this.jdbcTemplate.queryForList(Query,resultParam.getRollNumber(),resultParam.getClassName());
        return rows;
    }

    public List<Map<String,Object>> fetchStudentInfo(Result resultParam){
        String Query = "select s.sname,s.rollno,s.regdate,s.sid,c.cname,c.section from students s join classes c on c.classid=s.classid where s.rollno=? and s.classid=?";
        List<Map<String,Object>> rows = this.jdbcTemplate.queryForList(Query,resultParam.getRollNumber(),resultParam.getClassName());
        return rows;
    }
}
