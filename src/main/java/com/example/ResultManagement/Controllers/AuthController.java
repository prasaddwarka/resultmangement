package com.example.ResultManagement.Controllers;

import com.example.ResultManagement.Models.User;
import com.example.ResultManagement.Services.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class AuthController{

    @Autowired
    private AuthService authService;

    @GetMapping({"/"})
    public String DashBoard(){
        return "Dashboard";
    }

    @GetMapping({"/login"})
    public String LoginPage(Model model){
        User userLogin = new User();
        model.addAttribute("userLogin",userLogin);
        return "LoginPage";
    }

    @PostMapping(path="/login")
    public String CheckCredential(@ModelAttribute("userLogin") User userLogin,Model model) throws Exception{
        try{
            if(authService.checkUser(userLogin)){
                return "redirect:/admin/dashboard";
            }else{
                model.addAttribute("LoginMessage","False");
                return "LoginPage";
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            model.addAttribute("LoginMessage","False");
            return "LoginPage";
        }
    }
}
