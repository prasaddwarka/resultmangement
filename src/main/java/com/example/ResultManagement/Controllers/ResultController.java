package com.example.ResultManagement.Controllers;

import com.example.ResultManagement.Models.Classes;
import com.example.ResultManagement.Models.Result;
import com.example.ResultManagement.Services.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@Controller
public class ResultController{

    @Autowired
    private ResultService resultService;

    @GetMapping({"/result"})
    public String result(Model model){
        List<Classes> classList = this.resultService.getAllClasses();
        Result result = new Result();
        model.addAttribute("allCourse",classList);
        model.addAttribute("resultParam",result);
        return "Result/resultLogin";
    }

    @PostMapping({"/result"})
    public String FetchResult(@ModelAttribute("resultParam") Result resultParam,Model model){
        try{
            List<Map<String,Object>> resultInfo = resultService.fetchResult(resultParam);
            List<Map<String,Object>> studentInfo = resultService.fetchStudentInfo(resultParam);
            model.addAttribute("resultInfo",resultInfo);
            model.addAttribute("studentInfo",studentInfo);
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "Result/ViewResult";
    }

}
