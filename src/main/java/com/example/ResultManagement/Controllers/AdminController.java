package com.example.ResultManagement.Controllers;

import com.example.ResultManagement.Models.*;
import com.example.ResultManagement.Services.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class AdminController {

    @Autowired
    private AdminService adminService;

    //Dashboard Mapping
    @GetMapping({"/admin/dashboard"})
    public String manageStudent(Model model){
        List<Student> students = this.adminService.getStudents();
        List<Classes> classList = this.adminService.getClasses();
        model.addAttribute("allCourses",classList);
        model.addAttribute("students",students);
        return "Admin/Admin_Dashboard";
    }

    /* Adding Class */
    @GetMapping({"/admin/addclass"})
    public String AddCourse(Model model){
        Classes classes = new Classes();
        model.addAttribute("classes",classes);
        return "Admin/Course/AddCourse";
    }

    @PostMapping({"/admin/addclass"})
    public String GetCourseData(@ModelAttribute("classes") Classes classes,HttpSession session){
        try{
            if(this.adminService.addclass(classes) > 0){
                session.setAttribute("message",new Message("success","Your Data is Successfully Added..."));
            }else{
                session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
        }
        return "Admin/Course/AddCourse";
    }

    /* Adding Subject To Classes */
    @GetMapping({"/admin/addclasssubject"})
    public String AddClassSubject(Model model){
        List<Subject> subject = this.adminService.getSubjects();
        List<Classes> classList = this.adminService.getClasses();
        ClassSub classsub = new ClassSub();
        model.addAttribute("classsub",classsub);
        model.addAttribute("allCourses",classList);
        model.addAttribute("subject",subject);
        return "Admin/ClassSubjects/classSubject";
    }

    @PostMapping({"/admin/addclasssubject"})
    public String AddClassSubjectData(@ModelAttribute("classsub") ClassSub classsub, HttpSession session){
        try{
            if(this.adminService.addclassSubjects(classsub) > 0){
                session.setAttribute("message",new Message("success","Your Data is Successfully Added..."));
            }else{
                session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
        }
        return "Admin/ClassSubjects/classSubject";
    }

    /* Adding Student */
    @GetMapping({"/admin/addstudent"})
    public String AddStudent(Model model){
        List<Classes> classList = this.adminService.getClasses();
        Student student = new Student();
        model.addAttribute("allCourses",classList);
        model.addAttribute("student",student);
        return "Admin/Student/AddStudent";
    }

    @PostMapping({"/admin/addstudent"})
    public String GetStudentData(@ModelAttribute("student") Student student,HttpSession session){
        try{
            if(this.adminService.addstudent(student) > 0){
                session.setAttribute("message",new Message("success","Your Data is Successfully Added...."));
            }else{
                session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
        }
        return "Admin/Student/AddStudent";
    }

    /* Adding result */
    @GetMapping({"/admin/addresult"})
    public String AddResult(Model model){
        List<Student> students = this.adminService.getStudents();
        List<Subject> subjects = this.adminService.getSubjects();
        List<Classes> classes = this.adminService.getClasses();
        Results result = new Results();
        model.addAttribute("result",result);
        model.addAttribute("students",students);
        model.addAttribute("subjects",subjects);
        model.addAttribute("classes",classes);
        return "Admin/Result/AddResult";
    }

    @PostMapping({"/admin/addresult"})
    public String addresult(@ModelAttribute("result") Results result, HttpSession session){
        try{
            if(this.adminService.addresult(result) > 0){
                session.setAttribute("message",new Message("success","Your Result is Successfully Added...."));
            }else{
                session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
        }
        return "Admin/Result/AddResult";
    }

    /* Update Result */
    @GetMapping({"/admin/updateresult"})
    public String UpdateResult(Model model){
        List<Student> students = this.adminService.getStudents();
        List<Subject> subjects = this.adminService.getSubjects();
        List<Classes> classes = this.adminService.getClasses();
        Results result = new Results();
        model.addAttribute("result",result);
        model.addAttribute("students",students);
        model.addAttribute("subjects",subjects);
        model.addAttribute("classes",classes);
        return "Admin/Result/UpdateResult";
    }

    @PostMapping({"/admin/updateresult"})
    public String updateresult(@ModelAttribute("result") Results result,HttpSession session){
        try{
            if(this.adminService.updateresult(result) > 0){
                session.setAttribute("message",new Message("success","Your Result is Successfully Updated.."));
            }else{
                session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
            e.printStackTrace();
            session.setAttribute("message",new Message("danger","Something Went Wrong! Try Again."));
        }
        return "Admin/Result/UpdateResult";
    }
}
